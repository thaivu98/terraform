resource "aws_key_pair" "keypair_test" {
  key_name   = "keypair-${var.env}"
  public_key = "${file("modules/ec2/sshkey/id_rsa.pub")}"
}
